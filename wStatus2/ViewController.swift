//
//  ViewController.swift
//  wStatus2
//
//  Created by William Shea on 4/10/15.
//  Copyright (c) 2015 William Shea. All rights reserved.
//

import UIKit

class ViewController: UITableViewController, UITextViewDelegate {

    var datePicker:UIDatePicker!
    var editingDueDate:Bool = false
    
    @IBOutlet weak var statusDetail: UITextView!
    @IBOutlet weak var statusTitle: UITextField!
    @IBOutlet weak var statusDateLabel: UILabel!
  
  
    @IBAction func clickSaved(sender: AnyObject) {
        StatusManager.sharedInstance.save(statusTitle.text, detail: statusDetail.text, dueDate: myDatePicker.date)
        // Handle return seque
        
    }
    
    @IBAction func didDateChange(sender: AnyObject) {
        
        statusDateLabel.text = NSDateFormatter.localizedStringFromDate(myDatePicker.date, dateStyle: .ShortStyle, timeStyle: .NoStyle )
    }
    @IBOutlet weak var myDatePicker: UIDatePicker!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        didDateChange(self)
        toggleDatePicker()
        setTextViewPlaceholderText()
 
    }
    
    
    private func toggleDatePicker(){
        editingDueDate = !editingDueDate
        myDatePicker.hidden = editingDueDate
        tableView.deselectRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 0), animated: false)
        tableView.beginUpdates()
        tableView.endUpdates()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
 
        if editingDueDate && indexPath.section == 0 && indexPath.row == 3  {
            return 0.0
        } else {
            return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        }
        
      }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch( indexPath.section, indexPath.row){
        case( 0, 2):
                toggleDatePicker()
        default:
            ()
        }
    }
    
    func setTextViewPlaceholderText(){
        self.statusDetail.text = "Add Detail"
        self.statusDetail.textColor = UIColor.lightGrayColor()
        self.statusDetail.delegate = self
        
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if statusDetail.textColor == UIColor.lightGrayColor() {
            statusDetail.text = nil
            statusDetail.textColor = UIColor.blackColor()
        }
        
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if statusDetail.text.isEmpty {
            statusDetail.text = "Add Detail"
            statusDetail.textColor = UIColor.lightGrayColor()
            
        }
        
    }
}

