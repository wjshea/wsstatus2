//
//  Comments.swift
//  wStatus2
//
//  Created by William Shea on 4/12/15.
//  Copyright (c) 2015 William Shea. All rights reserved.
//

import Foundation
class Comment {
    var name:String
    var description:String
    
    init(name:String, description:String ){
        self.name = name
        self.description = description
        
    }
}


class CommentManager {
    
    class var sharedInstance:CommentManager{
        return _commentManager
    
    }
    
}

private let _commentManager = CommentManager()