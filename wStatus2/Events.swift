//
//  Events.swift
//  wStatus2
//
//  Created by William Shea on 4/12/15.
//  Copyright (c) 2015 William Shea. All rights reserved.
//

import Foundation

class Event {
    var date:NSDate!
    var name:String
    var description:String
    var project:String
    var show:Bool
    var type:String
    var importance:Int
    var owner:String
    
    init(name:String, description:String, project:String, type:String, importance:Int, show:Bool, owner:String, date:NSDate!) {
        
        self.name = name
        self.description = description
        self.project = project
        self.show = show
        self.type = type
        self.importance = importance
        self.owner = owner
        self.date = date
        
        
        
    }
}

class EventManager {
    
    class var sharedInstance:EventManager{
        return _eventManager
    
    }
    
}

private let _eventManager = EventManager()
