//
//  statusManager.swift
//  wStatus2
//
//  Created by William Shea on 4/15/15.
//  Copyright (c) 2015 William Shea. All rights reserved.
//

import Foundation
import CoreData
import UIKit

// This is just a helper class to wrap core data

class StatusManager : NSObject {
    
    var status = [NSManagedObject]()
    
    class var sharedInstance:StatusManager{
        return _statusManager
    
    }
    
    func count() -> Int
    {
        return status.count
    }
    
    func getAt(index:Int)->NSManagedObject {
        return status[index]
        
    }
    
    func fetch() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"StatusItem")
        
        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            status = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
    }
    
    func save(name:String, detail:String, dueDate:NSDate?){
    
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = delegate.managedObjectContext
        
        let entity = NSEntityDescription.entityForName("StatusItem", inManagedObjectContext: managedContext! )
        
        let statusItem = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        statusItem.setValue(name, forKey: "name")
        statusItem.setValue(detail, forKey: "detail")
        statusItem.setValue(dueDate, forKey: "dueDate")
        // Project 
        // Type - This will allow new fields
        var error:NSError?
        if ( !managedContext!.save(&error)){
            println("Error")
        }
        
        status.append(statusItem)
        
    }
    
}

private let _statusManager = StatusManager()

