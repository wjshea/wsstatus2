//
//  achievements.swift
//  wStatus2
//
//  Created by William Shea on 4/12/15.
//  Copyright (c) 2015 William Shea. All rights reserved.
//

import Foundation

class Achievements{
    
    var name:String
    var description:String
    var date:NSDate!
    var members:[String]!
    var show:Bool

    init(name:String, description:String, date:NSDate!, show:Bool){
        self.name = name
        self.description = description
        self.date = date
        self.show = show
    }
    
}

class AcheivementsManager {
    
    class var sharedInstance:AcheivementsManager{
        return _acheivementsManager
    
    }
    
}

private let _acheivementsManager = AcheivementsManager()