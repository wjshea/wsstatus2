//
//  StatusTableView.swift
//  wStatus2
//
//  Created by William Shea on 4/12/15.
//  Copyright (c) 2015 William Shea. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class StatusTableViewController : UITableViewController
{
    
    
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        StatusManager.sharedInstance.fetch()
    }
    override func viewDidLoad() {
        println("viewDidLoad")
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "myCell")
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("myCell") as! UITableViewCell
        println(indexPath.row)
        let item = StatusManager.sharedInstance.getAt(indexPath.row)
        // this should probably be rewrapped with a domain class
        cell.textLabel?.text = item.valueForKey("name") as! String?
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StatusManager.sharedInstance.count()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
 /*   override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let frame =  tableView.frame
        let buttonFrame = CGRectMake(frame.size.width-60, 10, 50, 30)
       
        let myButton = UIButton(frame: buttonFrame)
        myButton.titleLabel?.text = "+"
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(50)
    } */
    
}