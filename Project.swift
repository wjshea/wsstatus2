//
//  Project.swift
//  wStatus2
//
//  Created by William Shea on 4/11/15.
//  Copyright (c) 2015 William Shea. All rights reserved.
//

import Foundation

class Project {
    var name:String
    var description:String
    var dueDate:NSDate!
    var type:Int
    
    init(name:String, description:String, dueDate:NSDate!, type:Int){
            self.name = name
            self.description = description
            self.dueDate = dueDate
            self.type = type
    }
}


class ProjectManager {
    
    class var sharedInstance: ProjectManager{
        return _projectManager
    
    }
    
}

private let _projectManager = ProjectManager()
